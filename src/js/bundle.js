window.$ = require('jquery');
window.jQuery = window.$;
window.debounce = require('debounce');
window.throttle = require('./libraries/throttle');
window.niceSelect = require('./libraries/jquery-nice-select/jquery.nice-select');

// Require our js files.
require('./offcanvas');
require('./accordion');
// require('./backToTop');
require('./stickyMenu');
require('./callout');
require('./carousel');

$('select').niceSelect();

